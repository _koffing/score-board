import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from './Header';
import Player from './Player';
import AddPlayer from './AddPlayer';

class ScoreBoard extends Component {
  constructor(props){
    super(props);

    this.lastId = 100;

    this.state = { 
      totalPoints: 0,
      players: this.props.player_data 
    };
  }

  setId = () => {
    this.lastId ++;
    return this.lastId;
  }

  setScore = (scoredirection ,id) => {
    this.setState({
      players:  this.state.players.map((player)=>{
        if(id === player.id){
          let newScore = (scoredirection === 'increase') ? player.score + 1 : player.score - 1;
          return {
            ...player,
            score: newScore
          }
        }
        return {
          ...player
        }
      })
    });
  }

  decrementCounter = (id) => {
    this.setScore('decrease', id);
  }

  incrementCounter = (id) => {
    this.setScore('increase', id);
  }

  handleAddPlayer = (name) => {
    var players = this.state.players;
    players.push({
      name: name,
      score: 0,
      id: this.setId()
    });
    this.setState({
      players: players
    });
  }

  removePlayer = (id) => {
    var players = this.state.players.filter((player) => player.id !== id);
    this.setState({
      players: players
    });
  }

  render() {
    return (
      <div className="scoreboard">
        <Header 
        title={this.props.title}
        players={this.state.players}
        />
        <div className="players">
          {this.state.players.map((player, i)=>{
            return <Player 
                    key={player.id} 
                    score={player.score} 
                    name={player.name} 
                    decrementCounter={() => this.decrementCounter(player.id)}
                    incrementCounter={() => this.incrementCounter(player.id)} 
                    removePlayer={() => { this.removePlayer(player.id) }}
                    />;
          })} 
        </div>
        <AddPlayer onAdd={this.handleAddPlayer} />
      </div>
    );
  }
}

ScoreBoard.propTypes = {
  title: PropTypes.string,
  player_data: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired
  })).isRequired
}

Player.defaultProps = {
  title: "Score Board"
}

export default ScoreBoard;
