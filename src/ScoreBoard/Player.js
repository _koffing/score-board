import React from 'react';
import PropTypes from 'prop-types';
import Counter from "./Counter";

function Player(props) {
    return (
        <div className="player">
            <div className="player-name">
                <a className="remove-player" onClick={props.removePlayer}>X</a> 
                {props.name}
            </div>
            <div className="player-score">
                <Counter 
                score={props.score}
                decrementCounter={props.decrementCounter}
                incrementCounter={props.incrementCounter}
                />
            </div>
        </div>
    );
}

Player.propTypes = { 
    name: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
    decrementCounter: PropTypes.func.isRequired,
    incrementCounter: PropTypes.func.isRequired,
    removePlayer: PropTypes.func.isRequired
}

export default Player;