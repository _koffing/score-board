import React from 'react';
import PropTypes from 'prop-types';

// class Counter extends Component {
//     constructor(props) {
//         super(props);
//         this.state = { score: this.props.initialState };
//         this.incrementScore = this.incrementScore.bind(this);
//         this.decrementScore = this.decrementScore.bind(this);
//     }

//     incrementScore() {
//         this.setState({
//             score: (this.state.score + 1)
//         });
//     }

//     decrementScore() {
//         if(this.state.score !== 0 ){
//             this.setState({
//                 score: (this.state.score - 1)
//             });
//         }
        
//     }

//     render() {
//         return ( 
//             <div className="counter">
//                 <button className="counter-action decrement" onClick={this.decrementScore}>-</button>
//                 <div className="counter-score">{this.state.score}</div>
//                 <button className="counter-action increment" onClick={this.incrementScore}>+</button>
//             </div>
//         );
//     }
// }

function Counter(props){
    return (
        <div className="counter">
            <button className="counter-action decrement" onClick={props.decrementCounter} >-</button>
            <div className="counter-score">{props.score}</div>
            <button className="counter-action increment" onClick={props.incrementCounter} >+</button>
        </div>
    );
}

Counter.propTypes = {
    score: PropTypes.number.isRequired,
    decrementCounter: PropTypes.func.isRequired,
    incrementCounter: PropTypes.func.isRequired
}

export default Counter;