import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AddPlayer extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: ""
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.onAdd(this.state.name);
        this.setState({ name: '' });
    }

    onNameChange = (e) => {
        this.setState({name: e.target.value});
    }

    render = () => {
        return (
            <div className="add-player-form">
                <form onSubmit={this.handleSubmit}>
                    <input type='text' value={this.state.name} placeholder="Add Player" onChange={this.onNameChange} />
                    <input type='submit' value="add player" />
                </form>
            </div>
        );
    }
}

AddPlayer.propTypes = {
    onAdd: PropTypes.func.isRequired
}

export default AddPlayer;